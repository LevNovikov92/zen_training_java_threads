package zen_training_java_thread;

public class TestThread extends Thread {
	static private boolean stop = false;
	
	public TestThread(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		System.out.println("Strat thread " + getName());
		int i;
		for(i=0; i<200000; i++) {
			System.out.println(getName() + ": Count=" + i);
			if(stop) {
				System.out.println(getName() + " terminate" + ": count="+i);
				break;
			}
		}
		stop = true;
		System.out.println(getName() + " terminate" + ": count="+i);
		super.run();
	}
}
