package zen_training_java_thread;

import java.awt.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {
	
	public static void main(String[] args) {
		//startThreadPriorityTest();
		//startSynchronizedTest();
		//startThreadPullTest();
		//startCallableTaskTest();
		startCountDownLatchTest();
	}

	private static void startCountDownLatchTest() {
		CountDownLatch latch = new CountDownLatch(3);
		for(int i = 0; i<3; i++) {
			Thread thread = new Thread(new LatchTask("Task " + i, latch));
			thread.start();
		}
		System.out.println("All tasks have been runned");
		try {
			latch.await();
			System.out.println("All tasks have been executed");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void startCallableTaskTest() {
		ArrayList<Future<Integer>> resultList = new ArrayList<Future<Integer>>();
		ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
		Random random = new Random();
		for(int i = 0; i<5; i++) {
			int number = random.nextInt(10);
			CallableTask task = new CallableTask(number);
			Future<Integer> result = threadPool.submit(task);
			resultList.add(result);
		}
		
		for(Future<Integer> future: resultList) {
			try {
				System.out.println("Result = " + future.get());
			} catch (InterruptedException e) {
				System.err.println(e.toString());
				e.printStackTrace();
			} catch (ExecutionException e) {
				System.err.println(e.toString());
				e.printStackTrace();
			}
		}
	}

	private static void startThreadPullTest() {
		int numberOfThreads = 4;
		ThreadPoolExecutor threadPool = 
				(ThreadPoolExecutor) Executors.newFixedThreadPool(numberOfThreads);
		for(int i = 0; i<6; i++) {
			String taskName = "Task "+i;
			RunableTask task = new RunableTask(taskName, 20-i);
			System.out.println("has been added: " + task.getName());
			threadPool.execute(task);
		}
	}

	private static void startSynchronizedTest() {
		int nums[] = {2,2,2,2,2,2};
		SyncThread thread1 = new SyncThread("Thread 1", nums);
		SyncThread thread2 = new SyncThread("Thread 2", nums);
		thread1.start();
		thread2.start();
	}

	private static void startThreadPriorityTest() {
		System.out.println("ThreadPriorityTest BEGIN");
		TestThread testThread1 = new TestThread("Thread 1");
		TestThread testThread2 = new TestThread("Thread 2");
		testThread1.setPriority(Thread.NORM_PRIORITY+2);
		testThread2.setPriority(Thread.NORM_PRIORITY);
		testThread1.start();
		testThread2.start();
		try {
			testThread1.join();
			testThread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("END");
	}

}
