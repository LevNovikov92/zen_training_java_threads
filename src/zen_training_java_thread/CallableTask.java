package zen_training_java_thread;

import java.util.concurrent.Callable;

public class CallableTask implements Callable<Integer>{
	private int mNumber;
	
	public CallableTask(int number) {
		mNumber = number;
	}
	@Override
	public Integer call() throws Exception {
		Integer result = mNumber*mNumber;
		return result;
	}

}
