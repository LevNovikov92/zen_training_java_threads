package zen_training_java_thread;

import java.util.concurrent.CountDownLatch;

public class LatchTask implements Runnable{
	private CountDownLatch mLatch;
	private String mName;
	
	public LatchTask(String name, CountDownLatch latch) {
		mLatch = latch;
		mName = name;
	}
	
	@Override
	public void run() {
		for(int i = 1; i<5; i++) {
			int prcComplete = 25*i;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Thread '" + mName + "': " + prcComplete + "% complete");
		}
		mLatch.countDown();
	}

}
