package zen_training_java_thread;

public class SyncThread extends Thread {
	private int[] mNums;
	private static SumArray sumArray = new SumArray();
	
	public SyncThread(String name, int[] nums) {
		super(name);
		mNums = nums;
	}
	@Override
	public void run() {
		synchronized (sumArray) {
			int sum = sumArray.getSumm(mNums);
			System.out.println(getName() + ": return " + sum);
		}
		super.run();
	}

}
