package zen_training_java_thread;

public class SumArray {
	private int mSum = 0;
	
	public int getSumm(int[] values) {
		mSum = 0;
		for(int i=0; i<values.length; i++) {
			mSum += values[i];
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Running total for " +
			Thread.currentThread().getName() +
			" is " + mSum);
		}
		
		
		return mSum;
	}
}
