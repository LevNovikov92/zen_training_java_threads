package zen_training_java_thread;

public class RunableTask implements Runnable {
	private String mName;
	private int mSecondsNumber;
	
	public RunableTask(String name, int secondsNumber) {
		mName = name;
		mSecondsNumber = secondsNumber;
	}
	
	public String getName() {
		return mName;
	}
	
	@Override
	public void run() {
		System.out.println("Thread " + getName() + " started");
		for(int i=0; i<mSecondsNumber; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.err.println(e.toString());
				e.printStackTrace();
			}
		}
		System.out.println("Thread " + getName() + " stopped");
	}

}
